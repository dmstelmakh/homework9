package it.nordicSchool.java.homework9;


import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;

public class ListMain {
    public static void main(String[] args) {
        System.out.println("Пожалуйста, введите номер операции.");
        System.out.println("1 - добавить элемент (в конец)");
        System.out.println("2 - добавить элемент (в позицию)");
        System.out.println("3 - удалить элемент");
        System.out.println("4 - получить размер списка");
        System.out.println("5 - получить значение элемента по позиции");
        System.out.println("6 - выйти из класса");
        Scanner scan = new Scanner(System.in);

        int operNumb = scan.nextInt();
        ListHub spisok = new ListHub();


        do {
            try {

                switch (operNumb) {
                    case 1:
                        System.out.println("Пожалуйста, введите элемент");
                        int number = scan.nextInt();
                        spisok.addLast(number);

                        System.out.println("Пожалуйста, введите номер операции.");
                        operNumb = scan.nextInt();
                        break;
//                    case 11:
//                        System.out.println(spisok);
                    case 2:
                        try {
                            System.out.println("   Введите номер позиции (0 - " + (spisok.size() - 1) + ")");
                            int index = scan.nextInt();
                            System.out.println("   Введите новое значение элемента");
                            int newNumber = scan.nextInt();
                            spisok.setElementAtPosition (index, newNumber);

                             System.out.println("Пожалуйста, введите номер операции.");
                            operNumb = scan.nextInt();

                        } catch (IndexOutOfBoundsException e) {
                            System.out.println("Введен неверный номер позиции. Максимальная позиция " + (spisok.size() - 1));
                            scan.nextLine();
                        }
                        break;
                    case 3:
                        try {
                            System.out.println("   Введите номер позиции (0 - " + (spisok.size() - 1) + ")");
                            int index = scan.nextInt();
                             spisok.deleteElement(index);

                            System.out.println("Пожалуйста, введите номер операции.");
                            operNumb = scan.nextInt();
                        } catch (IndexOutOfBoundsException e) {
                            System.out.println("Введен неверный номер позиции. Максимальная позиция " + (spisok.size() - 1));
                            scan.nextLine();
                        }
                        break;
                    case 4:
                        System.out.println("Размер списка = " + spisok.size());
                        System.out.println("Пожалуйста, введите номер операции");
                        operNumb = scan.nextInt();
                        break;
                    case 5:
                        try {
                            System.out.println("   Введите номер позиции");
                            int index = scan.nextInt();

                            System.out.println("   Элемент позиции " + index + " - " + spisok.getElementByIndex(index));
                            System.out.println("Пожалуйста, введите номер операции");
                            operNumb = scan.nextInt();
                        } catch (IndexOutOfBoundsException e) {
                            System.out.println("      Элемент операции - не корректрен. Максимальный элемент " + (spisok.size() - 1));
                            scan.nextLine();
                        }
                        break;
                    default:
                        System.out.println("Операция не распознана");
                        System.out.println("Пожалуйста, введите номер операции");
                        operNumb = scan.nextInt();
                        break;
                }
            } catch (InputMismatchException e) {
                System.out.println("      Введенное значение должно быть Int");
                scan.nextLine();
            }
        } while (operNumb != 6);
        System.out.println("Вы вышли! ");
    }
}
