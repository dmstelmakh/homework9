package it.nordicSchool.java.homework9;

import java.util.LinkedList;

public class ListHub<E> implements Linked<E> {
    private Hub<E> firstHub;
    private Hub<E> lastHub;
    private int size = 0;

    public ListHub() {
        lastHub = new Hub<E>(null, firstHub, null);
        firstHub = new Hub<E>(null, null, lastHub);

    }

    @Override
    public void addLast(E e) {
        Hub<E> prev = lastHub;
        prev.setCurrentElement(e);
        lastHub = new Hub<E>(null, prev, null);
        prev.setNextElement(lastHub);
        size++;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public E getElementByIndex(int counter) {
        Hub<E> pocket = firstHub.getNextElement();
        for (int i = 0; i < counter; i++) {
            pocket = pickUpElement(pocket);
        }
        return pocket.getCurrentElement();
    }

    private Hub<E> pickUpElement(Hub<E> current) {
        return current.getNextElement();
    }

    @Override
    public void setElementAtPosition(int counter, E e) {
        Hub<E> pocket = firstHub.getNextElement();
        for (int i = 0; i < counter; i++) {
            pocket = pickUpElement(pocket);
        }
        pocket.setCurrentElement( e);

    }

    @Override
    public void deleteElement(int counter) {
        Hub<E> pocket = firstHub.getNextElement();
        for (int i = 0; i < counter; i++) {
            pocket = pickUpElement(pocket);
        }
        pocket.setCurrentElement((E) pocket.getNextElement());
    }

    private class Hub<E> {
        private E currentElement;
        private Hub<E> nextElement;
        private Hub<E> previousElement;

        private Hub(E currentElement, Hub<E> previousElement, Hub<E> nextElement) {
            this.currentElement = currentElement;
            this.previousElement = previousElement;
            this.nextElement = nextElement;
        }

        public E getCurrentElement() {
            return currentElement;
        }

        public void setCurrentElement(E currentElement) {
            this.currentElement = currentElement;
        }

        public Hub<E> getNextElement() {
            return nextElement;
        }

        public void setNextElement(Hub<E> nextElement) {
            this.nextElement = nextElement;
        }

        public Hub<E> getPreviousElement() {
            return previousElement;
        }

        public void setPreviousElement(Hub<E> previousElement) {
            this.previousElement = previousElement;
        }
    }
}
